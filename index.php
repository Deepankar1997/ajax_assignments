<!DOCTYPE html>
<html>
<body>

<div id="demo">
  <h2>Choose your preference.</h2>
  <p>Button 1 to handle GET Request.</p>
  <p>Button 2 to handle POST Request.</p>
  <p>Button 3 to update query.</p>
  <button type="button" onclick="handle_get()">Button 1</button>
  <button type="button" onclick="handle_post()">Button 2</button>
  <button type="button" onclick="handle_update_query()">Button 3</button>
</div>

</body>
</html>

<?php
$servername = "localhost";
$username = "deepankar";
$password = "mindfire";
$dbname = "dks";
$service=$_REQUEST['q'];
class DataEntry
{
	protected $conn;
	public function __construct($servername, $username, $password, $dbname)
	{
		try 
		{
			$dsn = "mysql:host=".$servername.";dbname=".$dbname.";";
			$this->conn = new PDO( $dsn, $username, $password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} 
		catch(PDOException $e) 
		{
			echo "Connection failed: " . $e->getMessage();
		}
	}

	//SELECT FUNCTION
	//The parameter $condition will take the column name in which  condition is to be applied(type=string).
	//The parameter $value will take the condition value(type=string). 
	public function select($table_name, $condition, $value)
	{
		$selection_value = array($value);
		$select_stmt = $this->conn->prepare("SELECT * FROM $table_name WHERE $condition = ?");
		try{
			$select_stmt->execute($selection_value);
			$select_stmt->setFetchMode(PDO::FETCH_OBJ);
			$result = $select_stmt->fetch();
			print_r($result);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	//SELECT ALL FUNCTION
	public function selectall($table_name){
		$sel=$this->conn->prepare("SELECT * FROM $table_name");
		try{
			$sel->execute();
			$sel->setFetchMode(PDO::FETCH_OBJ);
			$result = $sel->fetchAll();
			print_r($result);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	//UPDATE FUNCTION
	//The parameter $new_data will take new values to be updated in form of an associated array.
	//The parameter $id will take the primary key column name(type=string).
	//The parameter $value will take the key value(type=string).
	public function update($table_name, $new_data, $id, $value)
	{
		if($new_data !== null){
			$data = array_values($new_data);
		}
		array_push($data, $value);
		$cols = array_keys($new_data);
		$updated_values = array();
		foreach ($cols as $col)
		{
			$updated_values[] = $col."=?";
		}
		$updated_value = implode(', ',$updated_values);
		$update_stmt = $this->conn->prepare("UPDATE $table_name SET $updated_value WHERE $id=?");
		try{
			$update_stmt->execute($data);
			echo "Success!!";
		}
		catch(PDOException $e){
			echo "Updation Failed due to ".$e->getMessage();
		}
	}

}

$obj=new DataEntry($servername, $username, $password, $dbname);
if($service == "1"){
	$obj->selectall("user_data");
}
if($service == "2"){
	$obj->select("user_data","Email","abc@rediffmail.com");
}
if($service == "3"){
	$data=array("name"=>"Anurag","age"=>"25","Contact"=>"9040505020","Email"=>"aupadhyay@rediffmail.com");
	$obj->update("user_data",$data,"Email","deep@gail.com");
}
?>
<script src="assets/js/custom/handler_functions.js"></script>